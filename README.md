# OpenPGP Key Replacement

This repository holds the source code for the [OpenPGP Key Replacement Internet Draft](https://datatracker.ietf.org/doc/draft-ietf-openpgp-replacementkey/).
This is a direct successor of, and is directly based upon, the previous [Key Replacement for Revoked Keys in OpenPGP Internet Draft](https://datatracker.ietf.org/doc/html/draft-shaw-openpgp-replacementkey).
Merge requests are welcome.

## List of documents

Included in this repository are the following documents:

* replacementkey.md
    Markdown source for the OpenPGP Key Replacement Internet Draft.
